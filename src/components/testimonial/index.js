import React, { Component, useState } from 'react'
import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

 import './testimonial.scss'

const testimonialData = [
    {
        id: 0,
       
        content:
            'Tom and Jerry is an American cartoon series about a hapless cats never-ending pursuit of a clever mouse. Tom is the scheming cat, and Jerry is the spunky mouse.The series was driven entirely by action and visual humour; the characters almost never spoke.',
    },
    {
        id: 1,
       
        content:
            'Experience the warmth of the best cotton to be ever woven into sheets.',
    },
   
]

class Testimonial extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            arrows: false,
            autoPlay: true,
            dotsClass: 'slide-dots',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                    },
                },
            ],
        }

        return (
            <>
                <Slider {...settings}>
                    
                    {testimonialData.map(slide => (
                        <div className="testimonial" key={slide.id}>
                            <div className="testimonial-matter" >
                           
                            <p className="testimonial-content">{slide.content}</p>
                           
                            
                            </div>
                           
                        </div>
                        
                    ))}
                    

                </Slider>
            </>
        )
    }
}

export default Testimonial;
