import React, { useState } from 'react';
import "./footer.scss";
const Footer= ( ) => {
    return(
        <div className='footer'>
            <div className='logo-icons'>
                <h2>logo</h2>
                 <h4>Follow Us</h4>
                 <div className='icons'>

                 </div>

            </div>
            <div className='aboutus'>
                <h2>ABOUT US</h2>
                <ul className='aboutus-matter'>
                    <li>
                        <a href='#'>Our Promise</a>
                    </li>
                    <li>
                        <a href='#'> Blog</a>
                    </li>
                    <li>
                        <a href='#'>In News</a>
                    </li>
                    <li>
                        <a href='#'>Contact us</a>
                    </li>
                    
                </ul>
            </div>
            <div className='help'>
                <h2>HELP</h2>
                <ul className='help-matter'>
                    <li>
                        <a href='#'>FAQs</a>
                    </li>
                    <li>
                        <a href='#'>Laundary Care</a>
                    </li>
                    <li>
                        <a href='#'>Track Order</a>
                    </li>
                    <li>
                        <a href='#'>Shippinig & Returns</a>
                    </li>
                    
                </ul>
            </div>
            <div className='help-2'>
                
                <ul className='help-matter-2'>
                    <li>
                        <a href='#'>Terms & Conditions</a>
                    </li>
                    <li>
                        <a href='#'>Contact Us</a>
                    </li>
                    <li>
                        <a href='#'>Privacy</a>
                    </li>
                    
                </ul>
            </div>
            <div className='signup'>
                <h2>Excited For Interesting Updates?</h2>
                <p>Sign up and be the first to know about our best deals and new products!</p>
                <form>
                <input type="email" id="email" name="email" placeholder="Enter your Email"/>
                 <button type="submit">Submit</button>
                </form>

            </div>

        </div>
    )
};
export default Footer;
