import React, { useState } from 'react';
import './header.scss';

const Header = () => {
    
    return (
        <header className="header">
            <div className="header-inner">
               
                <a className="logo">
                   <h2>logo</h2>
                </a>
                <ul className="header-links">
                    <li>
                        <a href="#">Sheets</a>
                    </li>
                    <li>
                        <a href="#">Bedding</a>
                    </li>
                    <li>
                        <a href="#">Our Promise</a>
                    </li>
                    <li>
                        <a href="#">In Media</a>
                    </li>
                    <li>
                        <a href="#">My Account</a>
                    </li>
                </ul>
                <ul className="header-icons-right">
                    <li>
                        <div className="free-shipping">
                            <span>Free shipping <br></br> in United States</span>
                        </div>
                        <div className='shipping-icon'>
                            
                                                    </div>
                    </li>
                    <li>
                        <div className="search-icon"></div>
                    </li>
                    <li>
                        <div className="cart-icon">
                            
                        </div>
                    </li>
                </ul>
            </div>
            {/* <div
                
                className='sidebar'

                onClick={e => {
                    toggleSidebar(false)
                }}
             >
                <div
                    className="sidebar-header"
                    onClick={e => {
                        e.stopPropagation()
                    }}
                >
                    <a className="logo"></a>
                </div>
                <div
                    className="sidebar-content"
                    onClick={e => {
                        e.stopPropagation()
                    }}
                >
                    
                </div>
            </div> */}
            <hr></hr>
        </header>
    )
}



export default Header;