import React, { useState } from 'react';
import './why.scss';
function myFunction () {
    
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");
   
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Read more";
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      btnText.innerHTML = "Read less";
      moreText.style.display = "inline";
    }
    
  };

const Why =( )=>{
    
    return(
        <div className='why'>
            <div  className='why-matter'>
            <h1>WHY</h1><br/>
            <h3> Our Product?</h3>
            <p>Tom and Jerry is an American cartoon series about a hapless cat's never-ending pursuit of a clever mouse.
                 Tom is the scheming cat, and Jerry is the spunky mouse. <span id="dots">...</span><span id="more">
                The series was driven entirely by action and visual humour; the characters almost never spoke. </span></p>
                <span onclick="myFunction()" id="myBtn">Read more ></span>
                </div>
                <div  className='why-video'>
                <iframe width="420" height="315"
                  src="https://www.youtube.com/watch?v=MUMCZZl9QCY">
                 </iframe>  
                </div>
        </div>
    )
}
export default Why;