
import React, { Component, useState } from 'react'
import Slider from 'react-slick';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

 import './banner.scss'

const sliderData = [
    {
        id: 0,
        title: 'Human Connection',
        content:
            'Discover original, handmade designs crafted to give your home a personality.',
    },
    {
        id: 1,
        title: 'Quality Above All',
        content:
            'Experience the warmth of the best cotton to be ever woven into sheets.',
    },
    {
        id: 2,
        title: 'Factory Direct',
        content:
            'Taste the affordability of luxurious in-house manufactured products.',
    },
]

class Banner extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            arrows: false,
            autoPlay: true,
            dotsClass: 'slide-dots',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        dots: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                    },
                },
            ],
        }

        return (
            <>
                <Slider {...settings}>
                    
                    {sliderData.map(slide => (
                        <div className="slider" key={slide.id}>
                            <div className="slider-matter" >
                            <h3 className="slider-title">{slide.title}</h3>
                            <p className="slider-content">{slide.content}</p>
                            <button className="slider-button">Shop Now</button>
                            
                            </div>
                            <div className='banner-img'>
                            <img src='https://picsum.photos/600/403'></img>
                           </div>
                        </div>
                        
                    ))}
                    

                </Slider>
            </>
        )
    }
}

export default Banner;
