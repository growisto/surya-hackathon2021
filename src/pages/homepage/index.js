import react from "react";
import { useEffect, useState } from "react";

import Banner from "../../components/banner";
import Footer from "../../components/footer";
import Header from "../../components/header";
import Testimonial from "../../components/testimonial";
import Why from "../../components/why";

import './homepage.scss';
const HomePage = (props) =>{
    return(
        <div>
           <Header/>
          

         <Banner/>
         <Why/>
         <div className="testimonials">  
         <h1>EXPERT TESTIMONIALS</h1>
           <Testimonial/>
           </div>
          <div className="moreproducts">
              <h1>More Products</h1>
              <div className="product-list">
                   <div className="product-1 product">
                        <img src='https://picsum.photos/400/300'></img>
                        <h3>Comforters</h3>
                   </div>
                   <div className="product-1 product">
                        <img src='https://picsum.photos/400/300'></img>
                        <h3>Duvet Covers</h3>
                   </div>
                   <div className="product-1 product">
                        <img src='https://picsum.photos/400/300'></img>
                        <h3>Quilts</h3>
                   </div>
              </div>
          </div>

          <hr></hr>
          <Footer/>

        </div>
       
    )
};

export default HomePage;